#  Hello and Welcome to Pick To Light's Official Documentation !

This part of the user manual will be focusing on the software behind the ESP8266, we will go through its proper functioning and how to add features in case changes occured to the workstation.

This project has been designed as follow: 
    Each Workstation is driven by one ESP8266 (nodemcu), and on each "line" containing the boxes, (when possible), one adressable LED strip is mounted, otherwise you will need to add depending on the disposition of the boxes and the length of the strip that you have.
    In order to code this part of the project we wanted to have something that is easily modifiable, since the design and number of boxes on a workstation might change. We wanted something standardized in a way since we might have to implement the system on another workstation so any update and change should be done in a smooth and efficient way. For that matter, we considered our environment like a house that should be automated with LEDs, therefore, the esphome module was a perfect fit. In fact, it is a system that is designed for home automation using ESP8266 (what we happen to be using) and have predefined actions like turning on and off lights, have a MQTT Client Component that can set up the MQTT connection to our broker. By using a specific topic to which the esp will be subsribed to and will listen for MQTT messages that will trigger an action.

### 1- ADD a Workstation

    Since the project has been designed to have one ESP8266 module per workstation, if you desire to add a workstation and therefore add an ESP8266 module, you will have to take this code, modify the name in the esphome block using the following template: esp-ptl-NB_OF_WORKSTATION (eg: esp-ptl-2 for workstation 2) and fix the parts with the leds depending on what you have.

### 2- ADD a Box 


    If you desire to add a box in a workstation, and therefore add a light in the project it will depend on the design that you will be adding :   
        1-  If you are only adding one LED Strip for one box, you won't be needing the partition plateform and only proceed using the plateform of your LED (fastled_clockless for example), set the chipset, the pins the number of leds to turn on (if addressable) and RGB orders and you are good to go!
        2- If you are adding one addressable LED strip for 3 boxes for example, you will have to add to the plateform of your led (see 1) the partition plateform. In fact, we are dividing our LED Strip (like shown in this code) so the same strip can be used as many smaller LED strips, but only make sure you name your leds in that way: ledNB eg: led1 led2.... and connect them the to right strip using the id.
    Note that the topic used to control the state of the leds is the command_topic and has a specific format: name_Of_ESP/light/led_name/command for example if we're on workstation 2 trying to turn on led 3 the command topic will be : esp-ptl-2/light/led3/command.




